# %% def funkce
def greeting(name="World"):
    print(f"Hello {name}!")


# %% pozdrav
if __name__ == "__main__":
    print(__name__)
    greeting()
