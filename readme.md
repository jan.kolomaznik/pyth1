Python - základy programování
=============================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Úvod do jazyka Python. 
V tomto uvodud se seznáníme se základy jazyka, jeho syntaxí, základními datovými typy a operacemi, které nám poskytují.
Také se podíváme na konstrukce pro řízení toku programu, jako jsou cykly a větvení.
Následně se naučíme definovat funkce a používat.
Poslední dvě témata jsou venována zpracování výjimek a prací s řetězci.

 
Náplň kurzu:
------------

- **[Základy programování v Pythonu](00-python.ipynb)**
- **[Pracovní prostředí](01-python.developement.ipynb)**
- **[Základy jazyka Python](02-python.basic.ipynb)**
- **[Datové typy](03-python.type.ipynb)**
- **[Datovy struktury](04-python.structure.ipynb)**
- **[Základní konstrukce jazyka](05-python.construction.ipynb)**
- **[Funkce](06-python.function.ipynb)**
- **[Chyby a výjimky](07-python.except.ipynb)**
- **[Práce se soubory](08-python.file.ipynb)**
- **[Co je objektově orientované programování v Pythonu?](09-python.object.ipynb)**
- **[Tvorba skriptů](10-python.script.ipynb)**
- **[Moduly](11-python.module.ipynb)**
- **[Testování v python](12-python.test.ipynb)**

