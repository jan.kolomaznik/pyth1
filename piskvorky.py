# %% Importy
from sys import exit
from math import inf

SYMBOLS = [" ", "X", "0"]
hrac_na_tahu = SYMBOLS[1]

# %%
def zobraz_papir(papir):
    """Zobrazi jiz zahrane taky (O,X) na herni plose"""
    radky, sloupce = zip(*papir)
    print("    ", end="")
    for s in range(min(sloupce)-1, max(sloupce)+2):
        print(f"{s:>3}", end=":")
    print()
    for r in range(min(radky)-1, max(radky)+2):
        print(f"{r:3}", end=":")
        for s in range(min(sloupce)-1, max(sloupce)+2):
            print(f"{papir.get((r,s), SYMBOLS[0]):^3}", end="|")
        print()

# zobraz_papir({
#     (0, 0): SYMBOLS[1],
#     (1, 10): SYMBOLS[2],
# })

# %%
def ziskej_hracuv_tah(papir, hrac_na_tahu):
    """zjistit od hrace kam chce tahnout = urceni volne neobsazene pozice"""
    while True:
        try:
            tah = input(f"Na tahu je {hrac_na_tahu}, zadej (radek sloupec): ")
            coordinaty = tuple(map(int, tah.split()))
            if tah not in papir:
                return coordinaty
            print(f"Pole {coordinaty} je již obsazeno")

        except:
            if " " not in tah:
                exit(0)
            print("Zadej souradnice pro tah.")

# ziskej_hracuv_tah({
#     (0, 0): SYMBOLS[1],
#     (1, 1): SYMBOLS[2],
# })
# %%
def zkontolu_jestli_tah_znamenal_vitezti(papir, pozice):
    """Zkontroluje jestli od zadane pozice nevznikla 5 v rade, sloupci nebo diagonale"""
    okoli = ([],[],[],[])
    for i in range(-4,5):
        okoli[0].append(papir.get((pozice[0]+i,pozice[1]+0), SYMBOLS[0]))
        okoli[1].append(papir.get((pozice[0]+0,pozice[1]+i), SYMBOLS[0]))
        okoli[2].append(papir.get((pozice[0]+i,pozice[1]+i), SYMBOLS[0]))
        okoli[3].append(papir.get((pozice[0]+i,pozice[1]-i), SYMBOLS[0]))
    smery = tuple(map("".join, okoli))
    line = papir[pozice] * 5
    return any(map(lambda smer: line in smer, smery))

# zkontolu_jestli_tah_znamenal_vitezti({
#     (0, 0): SYMBOLS[1],
#     (1, 1): SYMBOLS[2],
#     (1, -1): SYMBOLS[1],
#     (2, -2): SYMBOLS[1],
#     (3, -3): SYMBOLS[1],
#     (4, -4): SYMBOLS[1],

# }, (0,0))

# %%
def urci_dalsiho_hrace(hrac_na_tahu):
    """Pokud je hrac O vraci X nebo naopak."""
    if hrac_na_tahu == SYMBOLS[1]:
        return SYMBOLS[2]
    else:
        return SYMBOLS[1]

# %% Herni smycka
# Zacinam definici 
# - jak vypada pocatecni rozlozeni na prazdnem papire 
# - kdo bude prvni hrac na tahu.
papir = {(0,0): SYMBOLS[1]}
hrac_na_tahu = SYMBOLS[2]

while len(papir) < 10:  # Chci povolit maximalne 100 platnych tahu
    zobraz_papir(papir)
    pozice = ziskej_hracuv_tah(papir, hrac_na_tahu)
    papir[pozice] = hrac_na_tahu
    if zkontolu_jestli_tah_znamenal_vitezti(papir, pozice):
        zobraz_papir(papir)
        print(f"Hrac {hrac_na_tahu} prave zvitezil")
        break
    hrac_na_tahu = urci_dalsiho_hrace(hrac_na_tahu)
else:
    print("Remiza. Konec hry!")



# %%
