# Základy programování v Pythonu

Python je programovací jazyk, který:
- Je pojmenovaný podle televizního pořadu *Monty Python’s Flying Circus* (BBC, 70. léta).
- Vznikl jako následovník jazyka ABC v 80. letech a byl navržen pro snadné použití i pro neprogramátory.
- Autorem je **Guido van Rossum**, který vytvořil Python v roce 1991.

![Guido van Rossum](https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSS-qxfUs6iQcBD7xoubC00i_ACfFogEIRJ45HDgondUyJJwriv)


## Hlavní charakteristiky Pythonu

- **Snadný na naučení:** Kód vypadá téměř jako psaná angličtina.
- **Rychlý vývoj:** Vhodný pro rychlé prototypování a každodenní úkoly.
- **Flexibilní:** 
  - Podpora více stylů programování (objektově orientovaný, funkcionální, imperativní).
  - Automatická správa paměti a dynamické typování.
- **Bohatá knihovna:** Obsahuje mnoho nástrojů a knihoven pro různé aplikace.
- **Moderní jazyk:** Dvě hlavní verze Pythonu (Python 2.x a 3.x), přičemž 3.x je aktuální standard.

## Co všechno lze s Pythonem dělat?

- **Automatizace a skripty:**
  - Automatizace každodenních úkolů a správa systémů (např. skripty v Linuxu).
- **Webové aplikace:**
  - Tvorba webových aplikací pomocí knihoven jako Django nebo Flask.
- **Strojové učení:**
  - Analýza dat a umělá inteligence (knihovny jako scikit-learn a numpy).
- **Uživatelská rozhraní (GUI):**
  - Tvorba grafických aplikací pomocí nástrojů jako Tkinter nebo PyQt.
- **Databáze:**
  - Práce s databázemi (MySQL, PostgreSQL, SQLite).
- **Integrace komponent:**
  - Propojování částí aplikací napsaných v jiných jazycích (např. pomocí Cythonu).
- **Síťová komunikace:**
  - Práce s protokoly FTP, Telnet, nebo zpracování XML a JSON.

## Kdo Python používá

- **Google:** Používá Python například pro YouTube a Google App Engine.
- **Dropbox:** Python je klíčovou součástí jejich klientských a serverových systémů.
- **Netflix:** Python využívají pro backendové systémy.
- **NASA:** Používají Python například v projektech spojených s Marsem.
- **Reddit:** Velká část infrastruktury je založena na Pythonu.
*[Python success](http://www.python.org/about/success)*

## Instalace a první kroky
  - Stažení Pythonu z [oficiálních stránek](https://www.python.org).
  - Použití správců balíčků (např. pip, základní použití).

### První spuštění
  - Použití interaktivního shellu (REPL).
  - Základní struktura Python programu.


```python
print("Ahoj, světe!")
```

Jako kalkulačka?


```python
a = 10
b = 3
print(a + b, 
      a - b, 
      a * b, 
      a / b)
```
